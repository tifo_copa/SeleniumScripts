package com.webtable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CommonFunctions {
	
	WebDriver driver = null;
	
	public CommonFunctions() {
		System.setProperty("webdriver.chrome.driver", "./executables/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}
	
	/**
	 * <p>Navigates to specified url</p>
	 * @param url
	 */
	public void navigate(String url) {
		driver.get(url);
	}
	
	/**
	 * Clicks on provided webElement
	 * @param elem
	 */
	public void clickElement(WebElement elem) {
		elem.click();
	}
	
	/**
	 * <p>Fetches content within a web element</p>
	 * @param elem
	 */
	public String getElementText(WebElement elem) {
		return elem.getText();
	}
	
	public String getElementText(String xpath) {
		return driver.findElement(By.xpath(xpath)).getText();
	}
	
	public int getCount(String xpath) {
		return driver.findElements(By.xpath(xpath)).size();
	}
	
	public void closeBrowser() {
		driver.close();
		driver.quit();
	}
	
	public String getExcelData(int row, int cell) throws IOException {
		FileInputStream fis = new FileInputStream(new File("./datasheet.xlsx"));
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheet("Sheet1");
		return sheet.getRow(row).getCell(cell).getStringCellValue();
	}
}
