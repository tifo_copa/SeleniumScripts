package com.webtable;

import java.io.IOException;

import org.testng.annotations.Test;

public class Script {
	
	@Test
	public void sampleTest() throws IOException {
		CommonFunctions comm = new CommonFunctions();
		comm.navigate(comm.getExcelData(1, 0));
		int rowCount = comm.getCount(comm.getExcelData(1, 1));
		for(int i=1; i<rowCount; i++) {
			int colCount = comm.getCount("//table[@id='table02']/tbody/tr["+i+"]/td");
			for(int j=1; j<colCount; j++) {
				String data = comm.getElementText("//table[@id='table02']/tbody/tr["+i+"]/td["+j+"]");
				System.out.println(data);
			}
		}
		comm.closeBrowser();
	}
}
