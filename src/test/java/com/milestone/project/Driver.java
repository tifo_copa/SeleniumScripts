package com.milestone.project;

import java.util.Scanner;

public class Driver {
	
	public static void main(String[] args) {
		Car santro = new Car("Hyundai", "i20", "htachback", 5, "1100cc", 0, 120);
		Transmission t = new Transmission("CVT", 1);
		santro.setTransmission(t);
		boolean running = true;
		int option = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("############### VEHICLE WITH AUTOMATIC TRANSMISSION #############");
		while(running) {
			System.out.println("Please select an option from below menu:");
			System.out.println("1. Start Vehicle");
			System.out.println("2. increase Speed");
			System.out.println("3. decrease Speed");
			System.out.println("4. apply brake");
			System.out.println("5. stop vehicle");
			option = sc.nextInt();
			switch(option) {
			case 1:
				santro.startVehicle();
				break;
				
			case 2:
				santro.increaseSpeed();
				break;
				
			case 3:
				santro.decreaseSpeed();
				break;
				
			case 4:
				santro.applyBrake();
				break;
				
			case 5:
				santro.stopVehicle();
				running = false;
				break;
				
			default:
				System.out.println("Please enter a valid option");
			}
		}
	}
}
