package com.milestone.project;

public class Car extends Vehicle{
	
	private int seatCapacity;
	private String engineCapacity;
	private int speed;
	private int maxSpeed;
	private boolean started;
	private Transmission transmission;
	
	public Transmission getTransmission() {
		return transmission;
	}

	public void setTransmission(Transmission transmission) {
		this.transmission = transmission;
	}

	public boolean isStarted() {
		return started;
	}

	public void setStarted(boolean started) {
		this.started = started;
	}

	public Car(String make, String model, String type, int seatCapacity, String engineCapacity, int speed, int maxSpeed) {
		super(make, model, type);
		this.seatCapacity = seatCapacity;
		this.engineCapacity = engineCapacity;
		this.speed = speed;
		this.maxSpeed = maxSpeed;
	}

	public int getSeatCapacity() {
		return seatCapacity;
	}

	public void setSeatCapacity(int seatCapacity) {
		this.seatCapacity = seatCapacity;
	}

	public String getEngineCapacity() {
		return engineCapacity;
	}

	public void setEngineCapacity(String engineCapacity) {
		this.engineCapacity = engineCapacity;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	
	public void increaseSpeed() {
		if(this.started) {
			if(this.getSpeed() < maxSpeed) {
				this.setSpeed(speed+=10);
				System.out.println("Gear changed to "+this.getTransmission().setGear(speed));
			}else {
				System.out.println("Maximum Speed Reached!");
			}
		}else {
			System.out.println("Please Start the Vehicle First");
		}
	}
	
	public void decreaseSpeed() {
		if(this.started) {
			if(this.getSpeed() > 10) {
				this.setSpeed(speed-=10);
				System.out.println("Gear changed to "+this.getTransmission().setGear(speed));
			}else {
				this.setSpeed(0);
				System.out.println("Car Standing still!");
			}
		}else {
			System.out.println("Please start the vehicle first");
		}
	}
	
	public void applyBrake() {
		if(this.getSpeed() > 30) {
			this.setSpeed(speed-=30);
			System.out.println("Gear changed to "+this.getTransmission().setGear(speed));
		}else {
			this.setSpeed(0);
			System.out.println("Car Standing still!");
		}
	}
}
