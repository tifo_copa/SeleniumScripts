package com.milestone.project;

public class Transmission {
	private String transmissionType;
	private int numberOfGears;
		
	public Transmission(String transmissionType, int numberOfGears) {
		this.transmissionType = transmissionType;
		this.numberOfGears = numberOfGears;
	}
	public String getTransmissionType() {
		return transmissionType;
	}
	public void setTransmissionType(String transmissionType) {
		this.transmissionType = transmissionType;
	}
	public int getNumberOfGears() {
		return numberOfGears;
	}
	public void setNumberOfGears(int numberOfGears) {
		this.numberOfGears = numberOfGears;
	}
	
	public int setGear(int speed) {
		if(speed > 0 && speed <=30) {
			this.setNumberOfGears(1);
			return this.getNumberOfGears();
		}else if(speed > 30 && speed <=60) {
			this.setNumberOfGears(2);
			return this.getNumberOfGears();
		}else if(speed > 60 && speed <=80) {
			this.setNumberOfGears(3);
			return this.getNumberOfGears();
		}else if(speed >80) {
			this.setNumberOfGears(4);
			return this.getNumberOfGears();
		}
		return 0;
	}
}
