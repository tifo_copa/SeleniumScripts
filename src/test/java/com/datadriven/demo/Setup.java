package com.datadriven.demo;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Setup {
	
	String expectedOutPut = "Hello";
	
	@Test
	public void test1() {
		Assert.assertEquals(expectedOutPut, "Hello");
		System.out.println("My Sample Test Logic");
	}
	
	@BeforeTest
	public void launchBrowser() {
		System.out.println("Before Test Logic goes here!");
	}
	
	@AfterTest
	public void cleanup() {
		System.out.println("After Test Cleanup Logic");
	}
	
}
